const socket = io()

const $messageForm = document.querySelector('#form-message')
const $messageFormInput = document.querySelector('input')
const $messageFormButton = document.querySelector('button')
const $sendLocationButton = document.querySelector('#send-location')
const $messages = document.querySelector('#messages')
const $chatSidebar = document.querySelector('#sidebar')

// Template
const messageTemplate = document.querySelector('#message-template').innerHTML
const locationTemplate = document.querySelector('#location-template').innerHTML
const sidebarTemplate = document.querySelector('#sidebar-template').innerHTML

// Options
const { username, room } = Qs.parse(location.search, { ignoreQueryPrefix: true})

const autoscroll = () => {
    // New message element
    const $newMessage = $messages.lastElementChild
    
    // Height of the new 
    const newMessageStyle = getComputedStyle($newMessage)
    const newMessageMergin = parseInt(newMessageStyle.marginBottom)
    const newMessageHeight = $newMessage.offsetHeight + newMessageMergin
    console.log(newMessageHeight)

    // Visiable height
    const visiableHeight = $messages.offsetHeight

    // Height of message container
    const containerHeight = $messages.scrollHeight

    // How far have I scrolled
    const scrollOffSet = $messages.scrollTop + visiableHeight

    if(containerHeight - newMessageHeight <= scrollOffSet) {
        $messages.scrollTop = $messages.scrollHeight 
    }
}

socket.on('message', (username, message) => {
    console.log(message)
    const html = Mustache.render(messageTemplate, {
        message: message.text,
        username,
        createAt: moment(message.createAt).format('LT')
    })
    $messages.insertAdjacentHTML("beforeend", html)
    autoscroll()
})

socket.on('locationMessage', (username, message) => {
    console.log(message.url)
    const html = Mustache.render(locationTemplate, {
        url: message.url,
        username,
        createAt: moment(message.createAt).format('LT')
    })
    $messages.insertAdjacentHTML('beforeend', html)
    autoscroll()
})

socket.on('roomData', ({room, users}) => {
    const html = Mustache.render(sidebarTemplate, {
        room,
        users
    })
    
    $chatSidebar.innerHTML = html
})

$messageForm.addEventListener('submit', (e) => {
    e.preventDefault()
    $messageFormButton.setAttribute('disabled', 'disabled')

    socket.emit('sendMessage', $messageFormInput.value, (error) => {
        $messageFormButton.removeAttribute('disabled')
        $messageFormInput.value = ''
        $messageFormInput.focus()

        if(error) {
            return console.log(error)
        }
        console.log('The message was delivered Successfully.')
    })
    
})

$sendLocationButton.addEventListener('click', () => {
    if(!navigator.geolocation) {
        return alert('Geolocation is not supported by your browser')
    }
    else {
        $sendLocationButton.setAttribute('disabled', 'disabled')

        navigator.geolocation.getCurrentPosition((position) => {
            console.log(position)
            const location = {
                latitude : position.coords.latitude,
                longitude : position.coords.longitude
            }

            socket.emit('sendLocation', location, () => {
                console.log('Location shared!')
                $sendLocationButton.removeAttribute('disabled')
            })
        })
    }
})

socket.emit('join', { username, room}, (error) => {
    if(error) {
        alert(error)
        location.href = '/'
    }
})
