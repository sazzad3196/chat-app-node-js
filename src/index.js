const express = require('express')
const path = require('path')
const http = require('http')
const socketio = require('socket.io')
const Filter = require('bad-words')
const  { generatedMessage, locationGeneratedMessage } = require('./utils/message')
const { addUser, getUser, removeUser, getUserInRoom } = require('./utils/user')

const app = express()
const server = http.createServer(app)
const io = socketio(server)

const port = process.env.PORT || 3000
const publicDirectoryPath = path.join(__dirname, '../public')

app.use(express.json())
app.use(express.static(publicDirectoryPath))
                                                                                                                       
io.on('connection', (socket) => {
    console.log('New webSocket connection')

    socket.on('join', ( options, callback ) => {
        const { error, user } = addUser({id: socket.id, ...options})

        if(error) {
            return callback(error)
        }

        socket.join(user.room)
        socket.emit('message', user.username, generatedMessage('Welcome!'))
        socket.broadcast.to(user.room).emit('message', user.username, generatedMessage(user.username + ' has joined'))
        io.to(user.room).emit('roomData', {
            room: user.room,
            users: getUserInRoom(user.room)
        })
        callback()
    })

    socket.on('sendMessage', (message, callback) => {
        const filter = new Filter()
        if(filter.isProfane(message)) {
            return callback('Profinity is not allowed.')
        }

        const user = getUser(socket.id)
        io.to(user.room).emit('message', user.username, generatedMessage(message))
        callback()
    })

    
    socket.on('sendLocation', (location, callback) => {
        const user = getUser(socket.id)
        io.to(user.room).emit('locationMessage', user.username, locationGeneratedMessage('https://google.com/maps?q=' + location.latitude + ',' + location.longitude))
        callback()
    })

    socket.on('disconnect', () => {
        const user = removeUser(socket.id)
        if(user) {
            io.to(user.room).emit('message', user.username, generatedMessage(user.username + ' has left!'))
        }
        io.to(user.room).emit('roomData', {
            room: user.room,
            users: getUserInRoom(user.room)
        })
    })
})

app.get('*', (req, res) => {
    res.send('Page not found.')
})

server.listen(port, () => {
    console.log('Server is up to port 3000.')
})