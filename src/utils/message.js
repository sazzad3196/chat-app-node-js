const generatedMessage = (text) => {
    return {
        text,
        createAt : new Date().getTime()
    }
}

const locationGeneratedMessage = (url) => {
    return {
        url,
        createAt : new Date().getTime()
    }
}

module.exports = {
    generatedMessage,
    locationGeneratedMessage
}